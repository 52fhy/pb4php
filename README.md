# pb4php

如何使用php解析proto文件？

## 先写一个proto文件

我们使用库里面提供的proto文件：test_new.proto：

``` proto
message Person
{
  required string name = 1;
  required int32 id = 2;
  optional string email = 3;


  enum PhoneType {
    MOBILE = 0;
    HOME = 1;
    WORK = 2;
  }


  message PhoneNumber {
    required string number = 1;
    optional PhoneType type = 2 [default = HOME];
  }
  // a simple comment
  repeated PhoneNumber phone = 4;
  optional string surname = 5;
}


message AddressBook {
  repeated Person person = 1;
}


message Test {
  repeated string person = 2;
}
```

## 生成解析后的文件

php不支持proto里的package，所以php版编译之前先要删掉package语句。

建立一个create_test_new.php文件存放编译命令：
``` php
<?php
require_once('../parser/pb_parser.php');
$parser = new PBParser();
$parser->parse('./test_new.proto');
echo 'ok;
```

结果在mytest目录下生成一个文件: `pb_proto_test_new.php`


### 运行实例

即运行test_new.php：

``` php
<?php
// first include pb_message
require_once('../message/pb_message.php');

// include the generated file
require_once('./pb_proto_test_new.php');

// generate message with the new definition with surname
// now just test the classes
$book = new AddressBook();
$person = $book->add_person();
$person->set_name('Nikolai');
$person = $book->add_person();
$person->set_name('Kordulla');
$person->set_surname('MySurname');


$phone_number = $person->add_phone();
$phone_number->set_number('0711');
$phone_number->set_type(Person_PhoneType::WORK);


$phone_number = $person->add_phone();
$phone_number->set_number('0171');
$phone_number->set_type(Person_PhoneType::MOBILE);


$phone_number = $person->add_phone();
$phone_number->set_number('030');


// serialize
$string = $book->SerializeToString();


// write it to disk
file_put_contents('test.pb', $string);

?>
```

test.pb是生成的二进制文件 基本结构一个字节类型+ 字节长度
